<?php

/**
 * Form callback for the manual import of an article.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function ntb_import_form($form, $form_state) {

  $node_type = variable_get('ntb_import_node_type');
  $uuid_field = variable_get('ntb_import_uuid_field');
  $disabled = !($node_type && $uuid_field);

  if ($disabled) {
    $internal_link = t('You need to <a href="@administer-page">configure NTB import settings</a> before you can import an article.', array(
      '@administer-page' => url('admin/config/services/ntb'),
    ));
    drupal_set_message($internal_link, 'warning');
  }

  $username = variable_get('ntb_import_username');
  $password = variable_get('ntb_import_password');
  $no_credentials = !($username && $password);
  if ($no_credentials) {
    $internal_link = t('You need to <a href="@administer-page">configure NTB authentication settings</a> before you can import an article.', array(
      '@administer-page' => url('admin/config/services/ntb/authentication'),
    ));
    drupal_set_message($internal_link, 'warning');
  }

  $form['ntb_id'] = array(
    '#type' => 'textfield',
    '#title' => t('ID'),
    '#disabled' => $disabled || $no_credentials,
    '#description' => t('The NTB ID of the article. This is NOT the uuid, it is the version ID, and it is expected to be an integer.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
    '#disabled' => $disabled || $no_credentials,
  );
  return $form;
}

/**
 * Submit handler for the manual article import form.
 *
 * @param $form
 * @param $form_state
 *
 */
function ntb_import_form_submit(&$form, &$form_state) {
  // Get the submitted ID.
  $ntb_id = $form_state['values']['ntb_id'];

  // Fetch the NITF XML from the NTB api.
  $article_source = _ntb_import_fetch_article_data($ntb_id);

  _ntb_import_import_article($article_source, $ntb_id);
}

/**
 * Configuration form callback: General settings.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function ntb_import_admin($form, &$form_state) {
  $stored_node_type = variable_get('ntb_import_node_type');
  $node_types = node_type_get_types();

  $node_types_options = array();
  foreach ($node_types as $machine_name => $bundle_data) {
    $node_types_options[$machine_name] = $bundle_data->name;
  }

  $form['ntb_import_node_type'] = array(
    '#type' => 'select',
    '#title' => 'Node type',
    '#options' => array('-' => '-') + $node_types_options,
    '#default_value' => $stored_node_type ? $stored_node_type : '-',
  );

  $ntb_import_uuid_field = variable_get('ntb_import_uuid_field');
  $form['ntb_import_uuid_field'] = array(
    '#type' => 'textfield',
    '#title' => 'NTB UUID field',
    '#default_value' => $ntb_import_uuid_field,
    '#required' => TRUE,
    '#description' => t('Type the machine name of a field. Please make sure that your selected node type actually has this field.'),
  );

  $form['ntb_import_uid'] = array(
    '#type' => 'textfield',
    '#title' => 'Owner user id',
    '#default_value' => variable_get('ntb_import_uid', 0),
    '#required' => TRUE,
    '#description' => t('The user that will be the owner of the imported nodes. If the uid does not exist, the anonymous user will be the owner.'),
  );

  $xlst_file = drupal_get_path('module', 'ntb_import') . '/includes/xslt/nitf-to-html.xsl';
  $form['ntb_import_xsl_path'] = array(
    '#type' => 'textfield',
    '#title' => 'Path to XSL file',
    '#default_value' => variable_get('ntb_import_xsl_path'),
    '#description' => t('Path to the XSL file that will be used for the conversion. If your module provides a custom XSL file, type the path here.<br />Leave it empty to use the default that is provided by the <i>NTB Import</i> module.<br />Example: <code>!xlst_file</code>', array('!xlst_file' => $xlst_file)),
  );

  return system_settings_form($form);
}

/**
 * Validation callback for general settings form.
 *
 * 1) Checks that an actual node type has been selected.
 * 2) Checks that the selected node type actually has a field with the typed
 * machine name.
 *
 * @param $form
 * @param $form_state
 *
 * @return bool
 */
function ntb_import_admin_validate(&$form, &$form_state) {
  $input = $form_state['values'];

  if ($input['ntb_import_node_type'] == '-') {
    form_set_error('ntb_import_node_type', t('You have to select a node type.'));
    return FALSE;
  }

  $ntb_import_uuid_field = $input['ntb_import_uuid_field'];
  $field_info_instance = field_info_instance('node', $ntb_import_uuid_field, $input['ntb_import_node_type']);
  if (!$field_info_instance) {
    form_set_error('ntb_import_uuid_field', t('There is no field with machine name !field on the node type !bundle that you selected.', array('!field' => $ntb_import_uuid_field, '!bundle' => $input['ntb_import_node_type'])));
    return FALSE;
  }

  $xsl_path = $input['ntb_import_xsl_path'];
  if ($xsl_path) {
    if (!file_exists($xsl_path)) {
      form_set_error('ntb_import_xsl_path', t('This file does not exist.'));
    }
  }
}

/**
 * Configuration form callback: Authentication settings.
 *
 * @return mixed
 */
function ntb_import_authentication() {
  $form['ntb_import_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('ntb_import_username'),
  );
  $form['ntb_import_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('ntb_import_password'),
  );
  return system_settings_form($form);
}
