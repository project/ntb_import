<?php

/**
 * Implements hook_menu().
 */
function ntb_import_menu() {
  $items = array();

  $items['admin/content/ntb-import'] = array(
    'title' => 'NTB import',
    'description' => 'Import NTB articles.',

    'page callback' => 'drupal_get_form',
    'page arguments' => array('ntb_import_form'),

    'file' => 'ntb_import.admin.inc',
    'access arguments' => array('import ntb articles'),
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
  );

  $items['admin/config/services/ntb'] = array(
    'title' => 'NTB import settings',
    'description' => 'Settings for NTB import.',

    'page callback' => 'drupal_get_form',
    'page arguments' => array('ntb_import_admin'),

    'file' => 'ntb_import.admin.inc',
    'access arguments' => array('administer ntb import'),
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/ntb/general'] = array(
    'title' => 'NTB import settings',
    'description' => 'Settings for NTB import.',
    'file' => 'ntb_import.admin.inc',
    'access arguments' => array('administer ntb import'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
  );
  $items['admin/config/services/ntb/authentication'] = array(
    'title' => 'Authentication',
    'description' => 'NTB authentication.',

    'page callback' => 'drupal_get_form',
    'page arguments' => array('ntb_import_authentication'),

    'file' => 'ntb_import.admin.inc',
    'access arguments' => array('administer ntb import'),
    'type' => MENU_LOCAL_TASK | MENU_NORMAL_ITEM,
    'weight' => 2,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function ntb_import_permission() {
  return array(
    'import ntb articles' => array(
      'title' => t('Import NTB articles'),
    ),
    'administer ntb import' => array(
      'title' => t('Administer NTB import'),
    ),
  );
}

/**
 * Helper function: Processes the xml source and maps the fields values.
 *
 * @param $article_source
 *
 * @param $article_revision_id
 *
 * @throws \EntityMetadataWrapperException
 */
function _ntb_import_import_article($article_source, $article_revision_id = NULL) {
  $node_type = variable_get('ntb_import_node_type');
  $uuid_field = variable_get('ntb_import_uuid_field');

  if (!$node_type || $uuid_field) {
    // @TODO handle this
  }

  $article_source_original = $article_source;

  // Step 1: Alter the article source (NITF XML, as it arrives).
  drupal_alter('ntb_import_xml', $article_source);

  // Step 2: Convert the NITF XML to HTML.
  $converted_html = _ntb_import_convert_nitf($article_source);

  // Step 3: Alter the converted HTML.
  drupal_alter('ntb_import_html', $converted_html);

  // Step 4: Map the fields (default mapping).
  $fields_mapping = _ntb_import_map_fields_default($article_source, $converted_html);

  // Step 5: Alter the mapping array before the node gets created.
  $context = array(
    'xml_altered' => $article_source,
    'html_altered' => $converted_html,
    'xml_original' => $article_source_original,
    'article_revision_id' => $article_revision_id,
  );
  drupal_alter('ntb_import_fields_mapping', $fields_mapping, $context);

  // Step 6: Do the actual import.
  $uuid = $fields_mapping[$uuid_field];
  $nid = ntb_import_find_node_by_uuid($uuid);
  $result = _ntb_import_insert_node($fields_mapping, $nid);
  if ($result['action'] == 'create') {
    $msg = t('A new node was created: <a href="/@node-link"><i>!title</i></a>', array(
      '@node-link' => drupal_get_path_alias('node/' . $result['node']->nid),
      '!title' => $result['node']->title,
    ));
  }
  else {
    $msg = t('The node: <a href="/@node-link"><i>!title</i></a> was updated.', array(
      '@node-link' => drupal_get_path_alias('node/' . $result['node']->nid),
      '!title' => $result['node']->title,
    ));
  }

  // Step 7: Let other modules know that a NTB article was imported.
  module_invoke_all('ntb_import_node_post_save', $result['action'], $result['node']);

  drupal_set_message($msg);
}

/**
 * Helper function: Wrapper function for importing the node.
 *
 * @param $fields_mapping
 * @param bool $nid
 *
 * @return array
 * @throws \EntityMetadataWrapperException
 */
function _ntb_import_insert_node($fields_mapping, $nid = FALSE) {
  $stored_owner_uid = variable_get('ntb_import_uid', 0);
  $owner = user_load($stored_owner_uid) === FALSE ? 0 : $stored_owner_uid;

  $node_type = variable_get('ntb_import_node_type');
  $uuid_field = variable_get('ntb_import_uuid_field');

  if (!$nid) {
    $node = _ntb_import_create_node($fields_mapping, $node_type, $owner);
    $return = array(
      'node' => $node,
      'action' => 'create',
    );
    return $return;
  }
  else {
    $node = _ntb_import_update_node($fields_mapping, $nid, $uuid_field);
    $return = array(
      'node' => $node,
      'action' => 'update',
    );
    return $return;
  }
}

/**
 * Creates new node.
 *
 * @param $fields_mapping
 * @param $node_type
 * @param $owner
 *
 * @return mixed
 * @throws \EntityMetadataWrapperException
 */
function _ntb_import_create_node($fields_mapping, $node_type, $owner) {
  // Creating new node.
  $e = entity_create('node', array('type' => $node_type));
  // Specify the author
  $e->uid = $owner;
  // Create a Entity Wrapper of that new Entity
  $entity_wrapper = entity_metadata_wrapper('node', $e);

  // Alter node data before inserting.
  drupal_alter('ntb_import_create_node', $entity_wrapper, $fields_mapping);

  foreach ($fields_mapping as $machine_name => $value) {
    $entity_wrapper->$machine_name = $value;
  }

  // Save the node.
  $entity_wrapper->save();
  return $entity_wrapper->value();
}

/**
 * Updates existing node.
 *
 * @param $fields_mapping
 * @param $nid
 * @param $uuid_field
 *
 * @return mixed
 * @throws \EntityMetadataWrapperException
 */
function _ntb_import_update_node($fields_mapping, $nid, $uuid_field) {
  // Updating existing node.
  // This field should now have the same value, but just to be sure we
  // explicitly unset it.
  unset($fields_mapping[$uuid_field]);

  $entity_wrapper = entity_metadata_wrapper('node', $nid);

  // Alter node data before updating.
  drupal_alter('ntb_import_update_node', $entity_wrapper, $fields_mapping);

  foreach ($fields_mapping as $machine_name => $value) {
    $entity_wrapper->$machine_name = $value;
  }

  // Save the node.
  $entity_wrapper->save();
  return $entity_wrapper->value();
}

/**
 * Returns the nid of the node that has the NTB uuid, or FALSE if not found.
 *
 * @param $uuid
 *
 * @return bool|int|null|string
 */
function ntb_import_find_node_by_uuid($uuid) {
  $uuid_field = variable_get('ntb_import_uuid_field');
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node')
    ->fieldCondition($uuid_field, 'value', $uuid, '=')
    ->range(0, 1)
    // Run the query as user 1.
    ->addMetaData('account', user_load(1))
  ;

  $result = $query->execute();
  if (isset($result['node'])) {
    $nid = key($result['node']);
    return $nid;
  }
  return FALSE;
}

/**
 * Helper function: Creates a DOMDocument object from the provided string.
 *
 * @param $data
 * @param string $document_type
 *
 * @return bool|\DOMDocument
 */
function _ntb_import_get_dom($data, $document_type = 'xml') {
  $dom = new DOMDocument;
  if ($document_type == 'xml') {
    $dom->loadXML($data);
  }
  elseif ($document_type == 'html') {
    $dom->loadHTML($data);
  }
  else {
    // @TODO throw Exception instead
    $dom = FALSE;
  }

  return $dom;
}

/**
 * Helper function: Returns DOMXPath object for the provided DOMDocument.
 *
 * @param $dom
 *
 * @return \DOMXPath
 */
function _ntb_import_get_xpath($dom) {
  $xpath = new DOMXPath($dom);
  return $xpath;
}

/**
 * Helper function: Maps the default fields:
 * 1) Title
 * 2) NTB uuid
 *
 * @param $article_source
 * @param $converted_html
 *
 * @return array
 */
function _ntb_import_map_fields_default($article_source, $converted_html) {
  $dom_xml = _ntb_import_get_dom($article_source, 'xml');
  $xpath_xml = _ntb_import_get_xpath($dom_xml);

  $map = array();

  // Title
  $article_title_query = "/nitf/head/title";
  $article_title = _ntb_import_get_first_value_from_xpath($article_title_query, $xpath_xml);
  $map['title'] = $article_title;

  // NTB uuid
  $ntb_import_uuid_field = variable_get('ntb_import_uuid_field');
  $ntb_uuid_query = "/nitf/head/meta[@name='NTBID']";
  $ntb_uuid = _ntb_import_get_attribute_value_from_xpath($ntb_uuid_query, $xpath_xml, 'content');
  $map[$ntb_import_uuid_field] = $ntb_uuid;

  return $map;
}

/**
 * Returns the value of first element that matches the xpath query.
 *
 * @param $query
 * @param $xpath_xml
 *
 * @return string
 */
function _ntb_import_get_first_value_from_xpath($query, $xpath_xml) {
  $length = $xpath_xml->query($query)->length;
  if (!$length) return '';

  $value = $xpath_xml->query($query)->item(0)->nodeValue;
  return $value;
}

/**
 * Returns the attribute value of first element that matches the xpath query.
 *
 * @param $query
 * @param $xpath_xml
 * @param $attribute_name
 *
 * @return string
 */
function _ntb_import_get_attribute_value_from_xpath($query, $xpath_xml, $attribute_name) {
  $length = $xpath_xml->query($query)->length;
  if (!$length) return '';

  $value = $xpath_xml->query($query)->item(0)->getAttribute($attribute_name);
  return $value;
}

/**
 * Helper function: Fetches the NITF XML from NTB's api.
 *
 * @param $ntb_id
 *
 * @return mixed
 */
function _ntb_import_fetch_article_data($ntb_id) {
  $curl = curl_init();

  $username = variable_get('ntb_import_username');
  $password = variable_get('ntb_import_password');

  curl_setopt($curl, CURLOPT_URL, "https://nyheter.ntb.no/ntbWeb/api/x1/doc/nitf/news/" . $ntb_id);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/xml', 'Accept: */*'));
  curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
  curl_setopt($curl, CURLOPT_TIMEOUT, 30);
  curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
  curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
  curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');

  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);
  if ($err) {
    // @TODO handle
  }

  return $response;
}

/**
 * Helper function: Converts the incoming NITF XML to HTML.
 * The XSL file is copied from @see https://iptc.org/standards/nitf/using-nitf/
 *
 * @param $xml_data_raw
 *
 * @return string
 */
function _ntb_import_convert_nitf($xml_data_raw) {
  $xsl_file = variable_get('ntb_import_xsl_path', FALSE);
  if (!$xsl_file) {
    // @see https://iptc.org/standards/nitf/using-nitf/
    $xsl_file = drupal_get_path('module', 'ntb_import') . '/includes/xslt/nitf-to-html.xsl';
  }
  $proc = new XsltProcessor;
  $domDocument = new DOMDocument();
  $domDocument->load($xsl_file);
  $proc->importStylesheet($domDocument);
  $domDocument->loadXML($xml_data_raw);
  $converted_obj = $proc->transformToDoc($domDocument);
  $converted = $converted_obj->saveHTML();
  return $converted;
}
